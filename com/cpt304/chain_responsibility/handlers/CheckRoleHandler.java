package com.cpt304.chain_responsibility.handlers;

public class CheckRoleHandler extends BaseHandler{

    @Override
    public boolean process(String username, String password) {
        if(username.startsWith("admin")){
            System.out.println("Load admin console");
        } else {
            System.out.println("Load user home page");
        }

        return handleNext(username, password);
    }
    
}
