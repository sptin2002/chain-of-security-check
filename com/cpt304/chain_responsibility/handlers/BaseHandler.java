package com.cpt304.chain_responsibility.handlers;

public abstract class BaseHandler {
    private BaseHandler next;

    public BaseHandler setNextHandler(BaseHandler next){
        this.next = next;
        return next;
    }

    public abstract boolean process(String username, String password);

    protected boolean handleNext(String username, String password){
        if(this.next == null){
            return true;
        }

        return next.process(username, password);
    }
}
