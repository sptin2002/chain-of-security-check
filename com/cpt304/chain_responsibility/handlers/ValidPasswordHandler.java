package com.cpt304.chain_responsibility.handlers;

import com.cpt304.chain_responsibility.data.UserModel;

public class ValidPasswordHandler extends BaseHandler{
    private UserModel userModel;

    public ValidPasswordHandler(UserModel userModel){
        this.userModel = userModel;
    }

    @Override
    public boolean process(String username, String password) {
        if(userModel.isValidPassword(username, password)){
            return handleNext(username, password);
        }

        System.out.println("Invalid password");
        return false;
    }
    
}
