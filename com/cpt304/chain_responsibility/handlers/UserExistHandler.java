package com.cpt304.chain_responsibility.handlers;

import com.cpt304.chain_responsibility.data.UserModel;

public class UserExistHandler extends BaseHandler {

    private UserModel userModel;

    public UserExistHandler(UserModel userModel){
        this.userModel = userModel;
    }

    @Override
    public boolean process(String username, String password) {
        if(userModel.isUserExist(username)){
            return handleNext(username, password);
        }

        System.out.println("User doesn't exist");
        return false;
    }
    
}
