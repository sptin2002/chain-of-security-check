package com.cpt304.chain_responsibility;

import com.cpt304.chain_responsibility.data.UserModel;
import com.cpt304.chain_responsibility.handlers.BaseHandler;
import com.cpt304.chain_responsibility.handlers.CheckRoleHandler;
import com.cpt304.chain_responsibility.handlers.UserExistHandler;
import com.cpt304.chain_responsibility.handlers.ValidPasswordHandler;
import com.cpt304.chain_responsibility.service.LoginService;

public class Main {
    public static void main(String[] argv) {
        UserModel userModel = new UserModel();

        BaseHandler handler = new UserExistHandler(userModel);
        handler.setNextHandler(new ValidPasswordHandler(userModel))
            .setNextHandler(new CheckRoleHandler());

        LoginService loginService = new LoginService(handler);
        loginService.login("user_username2", "password3");
    }
}