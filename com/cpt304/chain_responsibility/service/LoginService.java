package com.cpt304.chain_responsibility.service;

import com.cpt304.chain_responsibility.handlers.BaseHandler;

public class LoginService {
    private BaseHandler handler;

    public LoginService(BaseHandler handle){
        this.handler = handle;
    }

    public boolean login(String username, String password){
        if(handler.process(username, password)){
            System.out.println("Authentication and authorization success");
            return true;
        }

        System.out.println("Authentication and authorization fail");
        return false;
    }
}
