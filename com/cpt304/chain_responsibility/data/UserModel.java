package com.cpt304.chain_responsibility.data;

import java.util.HashMap;
import java.util.Map;

public class UserModel {
    private Map<String, String> users;

    public UserModel(){
        users = new HashMap<>();
        users.put("admin_username1","password1");
        users.put("user_username2","password2");
    }

    public boolean isUserExist(String username){
        return users.containsKey(username);
    }

    public boolean isValidPassword(String username, String password){
        return users.get(username).equals(password);
    }
}
